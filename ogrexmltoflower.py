'''
Created on 28.11.2012

@author: Farid
'''


import xml.etree.ElementTree as xml
import sys, getopt
import os
import StringIO


'''
Data models
'''
##################################################
##################################################


class Vector(object):

    def __init__(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0

##################################################

    def to_flower(self):
        return str(self.x) + ',' + str(self.y) + ',' + str(self.z)
    
##################################################
    
    def to_flower_vertices(self):
        return str('vertices ') + str(self.x) + ',' + str(self.y) + ',' + str(self.z) + '\n'


##################################################
##################################################


class Texcoord(object):

    def __init__(self):
        self.u = 0.0
        self.v = 0.0

##################################################

    def to_flower(self):
        return str(self.u) + ', ' + str(self.v)


##################################################
##################################################


class Vertex(object):

    def __init__(self):
        self.position = Vector()
        self.normal   = Vector()
        self.texcoord = Texcoord()


##################################################
##################################################


class Face(object):

    def __init__(self):
        self.v1 = 0.0
        self.v2 = 0.0
        self.v3 = 0.0

##################################################

    def to_flower(self):
        return str(self.v1) + ',' + str(self.v2) + ',' + str(self.v3) + ', '

##################################################

    def to_flower_last(self):
        return str(self.v1) + ',' + str(self.v2) + ',' + str(self.v3)


##################################################
##################################################

        
vertexes      = []
indexes       = []
materials     = []


##################################################
##################################################


def parse_vertices(rootElement):
    sharedgeometryList = rootElement.findall('sharedgeometry')
    if len(sharedgeometryList):
        sharedgeometry = sharedgeometryList[0]
        vertexbufferList = sharedgeometry.findall('vertexbuffer')
        if len(vertexbufferList):
            vertexbuffer = vertexbufferList[0]
            vertexList = vertexbuffer.findall('vertex')
            for vertex in vertexList:
                # First we create vertex object from our models
                vertexObj = Vertex()
                positionList = vertex.findall('position')
                if len(positionList):
                    position = positionList[0]
                    
                    y = float(position.get('y'))
                    x = float(position.get('x'))
                    z = float(position.get('z'))
                    
                    # set the position vector
                    vertexObj.position.x = x
                    vertexObj.position.y = y
                    vertexObj.position.z = z
                    #########################

                normalList = vertex.findall('normal')
                if len(normalList):
                    normal = normalList[0]
                    
                    y = float(normal.get('y'))
                    x = float(normal.get('x'))
                    z = float(normal.get('z'))
                    
                    # Set the normal vector
                    vertexObj.normal.x = x
                    vertexObj.normal.y = y
                    vertexObj.normal.z = z
                    #######################

                texcoordList = vertex.findall('texcoord')
                if len(texcoordList):
                    texcoord = texcoordList[0]
                    u = float(texcoord.get('u'))
                    v = float(texcoord.get('v'))
                    
                    # Set texturecoord 
                    vertexObj.texcoord.u = u
                    vertexObj.texcoord.v = v
                    #######################
                    
                vertexes.append(vertexObj)


##################################################
##################################################


def parse_indexes(rootElement):
    submeshesList = rootElement.findall('submeshes')
    if len(submeshesList):
        submeshes = submeshesList[0]
        submeshList = submeshes.findall('submesh')
        if len(submeshList):
            submesh = submeshList[0]
            facesList = submesh.findall('faces')
            if len(facesList):
                faces = facesList[0]
                faceList = faces.findall('face')
                for face in faceList:
                    faceObj = Face()
                    
                    faceObj.v1 = int(face.get('v1'))
                    faceObj.v2 = int(face.get('v2'))
                    faceObj.v3 = int(face.get('v3'))
                    
                    indexes.append(faceObj)


##################################################
##################################################


def write_parsed_data_file(fileName, meshType):
    flowerFile = open(fileName, 'w')
    
    flowerFile.write('#' + fileName +' converted by OgreXMLToFlower\n\n')
    flowerFile.write('type ' + meshType + '\n\n')
    
    materials = []
    
    for vertex in vertexes:
        flowerFile.write(vertex.position.to_flower_vertices())
        materials.append('0')
    
    stringIndexes = StringIO.StringIO()
    stringIndexes.write('indexes ')
    last_face = indexes.pop()
    for face in indexes:
        stringIndexes.write(face.to_flower())
    
    stringIndexes.write(last_face.to_flower_last())
        
    stringMaterials = StringIO.StringIO()
    stringMaterials.write('materials ')
    for material in materials:
        stringMaterials.write(material + ', ')
        
    stringMaterials.write('0')
        
    flowerFile.write('\n\n')
    
    flowerFile.write(stringIndexes.getvalue())
    
    flowerFile.write('\n\n')
    
    #flowerFile.write(stringMaterials)
    stringIndexes.close()
    flowerFile.close()

    print 'CONVERSION DONE!'


##################################################
##################################################


CONSOLE_HELP_TEXT = 'ogrexmltoflower.py -t <meshtype> -i <inputfile> -o <outputfile> -m <materialsfile>'


def main(argv):
    inputfile        = ''
    outputfile       = ''
    materialsFile    = ''
    meshType         = ''
    try:
        opts, args = getopt.getopt(argv,'ht:i:o:m:',['tType=', 'ifile=','ofile=', 'mfile'])
        if len(args):
            exit_with_help()
            
    except getopt.GetoptError:
        exit_with_help()
        
    for opt, arg in opts:
        if opt == '-h':
            exit_with_help()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ('-m', '--mfile'):
            materialsFile = arg
        elif opt in ('-t', '--type'):
            meshType = arg
    
    print 'Mesh type ',      meshType
    print 'Input file ',     inputfile
    print 'Output file ',    outputfile
    print 'Materials file ', materialsFile
    
    if not len(inputfile) or meshType not in ['convex', 'triangle', 'softbody']:
        exit_with_help()
    
    if not len(outputfile):
        outputfile = os.path.splitext(inputfile)[0]
    
    tree = xml.parse(inputfile)
    rootElement = tree.getroot()
    
    parse_vertices(rootElement)
    parse_indexes(rootElement)
    write_parsed_data_file(outputfile, meshType)


def exit_with_help():
    print CONSOLE_HELP_TEXT
    sys.exit(2)


##################################################
##################################################


if __name__ == '__main__':
    main(sys.argv[1:])
    pass

